#!/bin/bash
TARGET=fireled
VERSION=0.4
DESTDIR=release
CHROMEDIRS="content locale skin"
CURRDIR=`pwd`

echo "Creating Build Directory: $DESTDIR "
mkdir -p $DESTDIR
echo "Copy files in Build Directory"
cp -rf install.rdf chrome.manifest README LICENSE icon.png defaults chrome $DESTDIR
cd $DESTDIR
cd chrome/
echo "Removing old jar content"
rm -rf $TARGET.jar
echo "Compressing chrome content to chrome/$TARGET.jar"
zip -r $TARGET.jar *
echo "Removing staging directory in chrome folder"
rm -rf $CHROMEDIRS
cd ..
echo "Creating XPI extension: $TARGET-$VERSION.xpi  "
zip -r ../$TARGET-$VERSION.xpi *
echo "XPI extension created in $CURRDIR/$TARGET-$VERSION.xpi"

