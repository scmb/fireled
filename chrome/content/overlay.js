var led = new Array();
	led[0] = 0;
	led[1] = 0;
	led[2] = 0;
	led[3] = 0;
var modelType={NONE:0, DIRECTIO:1};
var	model;

var strings;
var noModel;
var noReading;
var error;
var noOS;
var inbox;
var osString;

var Fireled = {
  consoleService: Components.classes["@mozilla.org/consoleservice;1"].getService(Components.interfaces.nsIConsoleService),
  
  onLoad: function() {
    this.initialized = true;
	model=modelType.DIRECTIO;
	strings = document.getElementById("fireled-strings");
	noModel = strings.getString('noModel');
	noReading = strings.getString('noReading');
	error = strings.getString('error');
	noOS = strings.getString('noOS');
	inbox = strings.getString('inbox');
	osString = Components.classes["@mozilla.org/xre/app-info;1"].getService(Components.interfaces.nsIXULRuntime).OS; 
  },

  
   onClose: function() {
    this.initialized = true;
	Fireled.off(1);
	Fireled.off(2);
	Fireled.off(3);
	Fireled.off(4);
  },

on: function(led_id){
	if(led_id > 0 && led_id < 5 )
	{
		return Fireled.led(led_id,1);
	}
	else
	{
		console.log("No such LED id["+led_id+"]");
		return -5;
	}
},

off: function(led_id){
	if(led_id > 0 && led_id < 5 )
	{
		return Fireled.led(led_id,0);
	}
	else
	{
		console.log("No such LED id["+led_id+"]");
		return -5;
	}
},

read: function(led_id) {
	if(led_id > 0 && led_id < 5 )
	{
		return Fireled.read_led(led_id,0);
	}
	else
	{
		console.log("No such LED id["+led_id+"]");
		return -5;
	}
	
},

/*This function  is activated only by on() and off(), and ti selects the model of the computer/led*/
led: function(led_id,led_value){
	var ret = -6;
	switch(model){
		case 1: ret = Fireled.led_direct_io(led_id,led_value); break;
		default: console.log(noModel); break;
	}
	return ret;
},

/*This function  is activated only by read(), and ti selects the model of the computer/led*/
read_led: function(led_id){
	var ret = -6;
	switch(model){
		case 1: ret = Fireled.read_led_direct_io(led_id); break;
		default: console.log(noModel); break;
	}
	return ret;
},


/*Led-function specific for DIRECTIO*/
read_led_direct_io: function(led_id,led_value){
	var ret = 0;
	var file = Components.classes["@mozilla.org/file/local;1"]
		.createInstance(Components.interfaces.nsILocalFile);
	var value = 0xFFFF;

	const DIR_SERVICE = new 
		Components.Constructor("@mozilla.org/file/directory_service;1","nsIProperties");
	try { 
		path=(new DIR_SERVICE()).get("ProfD", Components.interfaces.nsIFile).path; 
	} catch (e) {
		console.log(error);
		ret = -3;
		return ret;
	}

	if(osString=="Linux"){
		file.initWithPath("/sys/class/leds/direct_io_gpio:"+led_id+"/brightness");
		var istream = Components.classes["@mozilla.org/network/file-input-stream;1"].
              		createInstance(Components.interfaces.nsIFileInputStream);
		istream.init(file, 0x01, 0444, 0);
		istream.QueryInterface(Components.interfaces.nsILineInputStream);

		// read lines into array
		var line = {}, lines = [], hasmore;
		do {
		  hasmore = istream.readLine(line);
		  lines.push(line.value); 
		} while(hasmore);

		istream.close();
		if(lines.length > 0)
		{
			value = parseInt(lines[0]);
			ret = 0;
		}
		else
		{
			ret = 1;
		}
	
	}
	else
	{
		console.log(noOS);
		ret = -4;
		return ret;
	}
	led[led_id-1] = value; 
	return ret;
},

/*Led-function specific for DIRECTIO*/
led_direct_io: function(led_id,led_value){
	var ret = 0;
	/*try {
		netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");
	} catch (e) {
		console.log(noReading);
		ret = -2;
		return ret;
	}*/
	var file = Components.classes["@mozilla.org/file/local;1"]
		.createInstance(Components.interfaces.nsILocalFile);

	const DIR_SERVICE = new 
		Components.Constructor("@mozilla.org/file/directory_service;1","nsIProperties");
	try { 
		path=(new DIR_SERVICE()).get("ProfD", Components.interfaces.nsIFile).path; 
	} catch (e) {
		console.log(error);
		ret = -3;
		return ret;
	}

	if(osString=="Linux"){
		file.initWithPath("/bin/sh");
		if(led_value!=0)
			var args = ["-c","echo 255 \> /sys/class/leds/direct_io_gpio:"+led_id+"/brightness "];
		else
			var args = ["-c","echo 0 \> /sys/class/leds/direct_io_gpio:"+led_id+"/brightness "];
		var process = Components.classes["@mozilla.org/process/util;1"].createInstance(Components.interfaces.nsIProcess);
		process.init(file);
		process.run(true, args, args.length);
		ret = parseInt(process.exitValue);
	}
	else
	{
		console.log(noOS);
		ret = -4;
		return ret;
	}
	return ret;
},

    browserEventListener: function(evt) {
	console.log("Received from web page: " + evt.target.getAttribute("ledId") + "/" + 
          evt.target.getAttribute("ledValue"));

	/* the extension answers the page*/
	evt.target.setAttribute("outputs", "0x00");
	
	var doc = evt.target.ownerDocument;
    	
	var AnswerEvt = doc.createElement("FireledAnswer");
    	AnswerEvt.setAttribute("response", "ok");
    	doc.documentElement.appendChild(AnswerEvt);
    	
	var event = doc.createEvent("HTMLEvents");
    	event.initEvent("FireledAnswerEvent", true, false);
    	AnswerEvt.dispatchEvent(event);
    },
   
    listen_request: function(callback) { // analogue of chrome.extension.onRequest.addListener
      document.addEventListener("fireled-request", function(event) {
        var node = event.target;
        if (!node || node.nodeType != Node.TEXT_NODE)
          return;

        var doc = node.ownerDocument;
        callback(JSON.parse(node.nodeValue), doc, function(response) {
          node.nodeValue = JSON.stringify(response);

          var event = doc.createEvent("HTMLEvents");
          event.initEvent("fireled-response", true, false);
          return node.dispatchEvent(event);
        });
      }, false, true);
    },

    callback: function(request, sender, callback) {
      if(request.ledId)
      {
	var ledId = request.ledId;
	var ret = 0;
	if(request.ledValue)
	{
		var ledValue = request.ledValue;
		ret = Fireled.on(ledId,ledValue);
	}
	
	if (ret!=0)
	{
		callback({respCode: ret, resp: "Error"});
	}
	else
	{
		ret = Fireled.read(ledId);

		if (ret!=0)
			callback({respCode: ret, resp: "Error"});
		else
			callback({respCode: 0, resp: "Ok", ledVal: led[ledId-1]});
	}
      }
      else
      {
	callback({respCode: -1, resp: "Error in parameters"});
      }

      return callback(null);
    }

};

var mainWindow = window.QueryInterface(Components.interfaces.nsIInterfaceRequestor)
                  .getInterface(Components.interfaces.nsIWebNavigation)
                  .QueryInterface(Components.interfaces.nsIDocShellTreeItem)
                  .rootTreeItem
                  .QueryInterface(Components.interfaces.nsIInterfaceRequestor)
                  .getInterface(Components.interfaces.nsIDOMWindow);

mainWindow.addEventListener("load", function(e) { Fireled.onLoad(e); }, false);
mainWindow.addEventListener("close", function(e) { Fireled.onClose(e); }, false); 
mainWindow.document.addEventListener("FireledEvent", function(e) { Fireled.browserEventListener(e); }, false, true);

Fireled.listen_request(Fireled.callback);

